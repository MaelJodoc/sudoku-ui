import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-sudoku',
  templateUrl: './sudoku.component.html',
  styleUrls: ['./sudoku.component.css']
})
export class SudokuComponent implements OnInit {
  private file: any;
  task: string[][];
  canDrawTask = false;
  mustDrawTask = false;
  private alreadyRead = false;
  private alreadySolved = false;
  private url = 'http://localhost:8080/sudoku/solveArr';

  constructor(private httpClient: HttpClient) {
    this.initTask();
  }

  ngOnInit() {
  }

  fileChanged(e) {
    this.file = e.target.files[0];
    this.alreadySolved = false;
    this.canDrawTask = false;
    this.mustDrawTask = false;
    this.alreadyRead = false;
  }

  showTask() {
    this.mustDrawTask = true;
    if (!this.alreadyRead) {
      this.readFile();
    }
  }

  solveTask() {
    if (!this.alreadySolved) {
      if (!this.alreadyRead) {
        if (this.file == null) {
          return;
        }
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
          this.readFromFileReader(fileReader);
          if (this.canDrawTask) {
            // отсылаем на сервер
            console.log('send on server');
            this.postTask().subscribe(response => {
              this.task = response;
              this.alreadySolved = true;
              this.showTask();
            });
          }
        };
        fileReader.readAsText(this.file);
      } else {
        if (this.canDrawTask) {
          // отсылаем на сервер
          console.log('send on server');
          this.postTask().subscribe(response => {
            this.alreadySolved = true;
            this.task = response;
            this.showTask();
          });
        }
      }
    }
  }

  private readFile() {
    if (this.file == null) {
      this.canDrawTask = false;
      this.mustDrawTask = false;
      this.alreadyRead = false;
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.readFromFileReader(fileReader);
    };
    fileReader.readAsText(this.file);
  }

  private readFromFileReader(fileReader) {
    let task: string = fileReader.result.toString();
    if (task.length > 500) {
      this.canDrawTask = false;
      this.mustDrawTask = false;
      this.alreadyRead = true;
      return;
    }
    task = task.replace(/\r?\n|\r/g, '');
    task = task.replace(/\s+/g, '');
    if (task.length !== 9 * 9) {
      this.canDrawTask = false;
      this.mustDrawTask = false;
      this.alreadyRead = true;
      return;
    }
    const taskAsSingleString: string[] = task.split('');
    for (let i = 0; i < taskAsSingleString.length; i++) {
      const rowIndex = parseInt((i / 9).toString(), 10);
      this.task[rowIndex][i % 9] = taskAsSingleString[i];
    }
    this.canDrawTask = true;
    this.alreadyRead = true;
    console.log(this.task);
  }

  private postTask(): Observable<string[][]> {
    return this.httpClient.post<string[][]>(this.url, this.task);
  }

  private initTask() {
    this.task = [];
    for (let i = 0; i < 9; i++) {
      this.task[i] = [];
      for (let j = 0; j < 9; j++) {
        this.task[i][j] = ' ';
      }
    }
  }
}
